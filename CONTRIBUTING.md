# Introduction

Hi new Royal Knight, if you are reading this file it means you are very excited to participate 
with this great idea that is create Web Components from Patternfly 4 HTML components, 
this is one of many Caballier wants to have available.

This is another open source project from Caballier community, we love to receive 
more hands to join us in our initiatives. As an open source community we cannot promise gold 
or lands by your services, but we promise highliht your name in each realease you had participated. 
As part of our army, you will be able to participate doing different chores or attacking different fronts. 
So, you can contribute from writing tutorials or blog posts, improving the documentation, 
submitting bug reports and feature requests or writing code which can be incorporated 
into this Web Component itself.

# Ground Rules

Keep in mind we expect your best because all of us have a great idea to give or 
another vision that generates value, but to have some base of how we work you just 
have to follow the instructions bellow:

- Be clair with questions and issues 